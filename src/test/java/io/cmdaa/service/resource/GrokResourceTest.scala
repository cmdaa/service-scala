package io.cmdaa.service.resource

import io.cmdaa.data.dao.{GrokRepo, UserProjectRepo}
import io.cmdaa.data.util.entity.GrokSchema.{Grok, GrokLogMessages}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfter, FlatSpec}

class GrokResourceTest extends FlatSpec with MockFactory with BeforeAndAfter {

  val grok = Grok(Some(1L), -1, "uuid", false, "TEST GROK", false, 1L)
//  val grok = Grok(Some(1L), active = true, "TEST GROK", 1L)
  val logMessage = GrokLogMessages(Some(1L),"Log Message", 1L, 1L)

  val listGroks = scala.List(grok, grok)
  val listLogMsgs = scala.List(logMessage, logMessage)

//  val logFile = LogFile(1L,"messages","/var/log/messages",1L)
//  val listLogFile = scala.List(logFile, logFile)
  val userProjectRepo: UserProjectRepo = stub[UserProjectRepo]
  val grokRepo: GrokRepo = stub[GrokRepo]

  val grokResource = new GrokResource(grokRepo, userProjectRepo)

//  def findAllGroks()
//  def findGrokById(@PathParam("id") id: Long)
//  def insertGrok(grok: Grok)
//  def insertGrok(groks: List[Grok])
//  def deleteGrokById(@PathParam("id") id: Long)
//  def deleteGrokByMetaDataId(@PathParam("metaDataId") metaDataId: Long)
//  def findAllGrokLogMsg()
//  def findGrokLogMsgById(@PathParam("id") id: Long)
//  def insertGrokLogMsg(logMessages: GrokLogMessages)
//  def insertGrokLogMsg(logMessages: List[GrokLogMessages])
//  def deleteGrokLogMsgById(@PathParam("id") id: Long)
//  def deleteGrokLogMsgByGrokId(@PathParam("id") grokId: Long)

  "findAllGroks" should "find all groks entered" in {
    (grokRepo.findAllGroks _).when().returns(listGroks)
    assert(grokResource.findAllGroks().getEntity == listGroks)
  }

  "findGrokById" should "find a single grok by a grok id" in {
    (grokRepo.findGrokById _).when(1).returns(Some(grok))
    assert(grokResource.findGrokById(1).getEntity == grok)
  }

  "insertGrok" should "insert a single grok" in {
    (grokRepo.insertGrok(_: Grok)).when(grok).returning()
    assert( ( grokResource insertGrok(grok) ).getEntity === () )
  }

  "insertGrokList" should "insert a list of groks" in {
    (grokRepo.insertGrok(_: List[Grok])).when(listGroks).returning()
    assert( ( grokResource insertGrok(listGroks) ).getEntity === () )
  }

  "deleteGrokById" should "delete grok by id" in {
    (grokRepo.deleteGrokById _).when(1).returning(1)
    assert(grokResource.deleteGrokById(1).getEntity == 1)
  }

  "deleteGrokByMetaDataId" should "delete grok by meta data id" in {
    (grokRepo.deleteGrokByMetaDataId _).when(1).returning(5)
    assert(grokResource.deleteGrokByMetaDataId(1).getEntity == 5)
  }

  "findAllGrokLogMsg" should "find all grok log message" in {
    (grokRepo.findAllGrokLogMsg _).when().returns(listLogMsgs)
    assert(grokResource.findAllGrokLogMsg().getEntity == listLogMsgs)
  }

  "findGrokLogMsgById" should "find grok log message by id" in {
    (grokRepo.findGrokLogMsgById _).when(1).returns(Some(logMessage))
    assert(grokResource.findGrokLogMsgById(1).getEntity == Some(logMessage))
  }

  "insertGrokLogMsg" should "insert a single log message" in {
    (grokRepo.insertGrokLogMsg(_: List[GrokLogMessages])).when(listLogMsgs).returning()
    assert( ( grokResource insertGrokLogMsg(listLogMsgs) ).getEntity === () )
  }

  "insertGrokLogMsgList" should "insert a list of log message" in {
    (grokRepo.insertGrokLogMsg(_: List[GrokLogMessages])).when(listLogMsgs).returning()
    assert( ( grokResource insertGrokLogMsg(listLogMsgs) ).getEntity === () )
  }

  "deleteGrokLogMsgById" should "delete grok log message by log message id" in {
    (grokRepo.deleteGrokLogMsgById _).when(1).returning(1)
    assert(grokResource.deleteGrokLogMsgById(1).getEntity == 1)
  }

  "deleteGrokLogMsgByGrokId" should "delete grok log message by grok id" in {
    (grokRepo.deleteGrokLogMsgByGrokId _).when(1).returning(5)
    assert(grokResource.deleteGrokLogMsgByGrokId(1).getEntity == 5)
  }
}
