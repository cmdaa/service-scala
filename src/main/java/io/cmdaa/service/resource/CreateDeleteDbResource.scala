package io.cmdaa.service.resource

import java.util.Calendar

import io.cmdaa.data.util.entity.LogFileSchema._
import io.cmdaa.data.util.entity.ServerSchema._
import io.cmdaa.data.util.entity.UserSchema._
import io.cmdaa.data.util.entity._
import io.cmdaa.data.util.entity.UserSchema.User
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response
import javax.ws.rs.{GET, Path, Produces}
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.JdbcProfile

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Path("/db-schema")
class CreateDeleteDbResource(val profile: JdbcProfile, val db: Database) {

  import profile.api._

  @GET
  @Path("create-fake-data")
  @Produces(Array(APPLICATION_JSON))
  def addFakeData(): Response = {
//    val db = Database.forConfig(dbType)

    val session = db.createSession()

    val now = Calendar.getInstance().getTime.getTime
    val action = DBIO.seq(
      users += User(Some(1), "admin", "sha1:64000:18:wWoPRCO95og1EPNUlh/C+WUB+2VTqapk:NL5kW+HhB4PnnVZJlMFWerJo", "The", "Administrator", "admin@foo.bar", active = true, approved = true, "admin", now),
      users += User(Some(2), "jdoe", "sha1:64000:18:wWoPRCO95og1EPNUlh/C+WUB+2VTqapk:NL5kW+HhB4PnnVZJlMFWerJo", "John", "Doe", "jdoe@test.com", active = true, approved = true, "admin", now),
      labels += Label(Some(1), "HTTP", "admin", now),
      labels += Label(Some(2), "Cloudera", "admin", now),
      servers += Server(Some(1), "one.foo.bar", "127.0.0.1", "::1", "admin", now),
      serverLabels += ServerLabel(1, 1),
      serverLabels += ServerLabel(1, 2),
      roles += Role(Some(1), "Superuser", "The superuser", "admin", now),
      roles += Role(Some(2), "Administrator", "The administrator", "admin", now),
      roles += Role(Some(3), "Owner", "The owner", "admin", now),
      roles += Role(Some(4), "Member", "The member", "admin", now),
      systems += System(Some(1), "System One", "This is system one", "admin", now),
      projects += Project(Some(1), "Project One", 1, 1, suggestRegEx = true, Some(60), Some("Minutes"), "PENDING", "admin", now),
      projects += Project(Some(2), "Project Two", 1, 1, suggestRegEx = false, None, None, "PENDING", "admin", now),
      userProjects += UserProject(2, 1, 4),
      userProjects += UserProject(2, 2, 4),
      logFiles += LogFile(1, "messages", "/var/log/apache", 1),
      logFiles += LogFile(2, "nginx", "/var/log/nginx", 1),
      logFiles += LogFile(3, "httpd", "/var/log/httpd", 1),
      logSourceTypes += LogSourceType(Some(1), "CMDAA", "{}", "admin", now),
      logSources += LogSource(Some(1), "messages", 1, Some(500), "{}", 1, "admin", now),
      logSources += LogSource(Some(2), "nginx", 1, Some(600), "{}", 1, "admin", now),
      logSources += LogSource(Some(3), "httpd", 1, Some(700), "{}", 2, "admin", now)

//      groks += Grok(Some(1), true, "Foo Bar Baz", 1),
//      groks += Grok(Some(2), true, "Foo Bar Baz", 1),
//      groks += Grok(Some(3), true, "Foo Bar Baz", 1),
//      groks += Grok(Some(4), true, "Foo Bar Baz", 2),
//      groks += Grok(Some(5), true, "Foo Bar Baz", 2),
//      grokLogMessages += GrokLogMessages( Some(1), "TEST LOG MESSAGE 1", 1, 1 ),
//      grokLogMessages += GrokLogMessages( Some(2), "TEST LOG MESSAGE 2", 1, 1 ),
//      grokLogMessages += GrokLogMessages( Some(3), "TEST LOG MESSAGE 3", 1, 1 ),
//      grokLogMessages += GrokLogMessages( Some(4), "TEST LOG MESSAGE 4", 1, 1 ),
//      grokLogMessages += GrokLogMessages( Some(5), "TEST LOG MESSAGE 5", 1, 1 ),
//      grokLogMessages += GrokLogMessages( Some(6), "TEST LOG MESSAGE 6", 2, 2 ),
//      grokLogMessages += GrokLogMessages( Some(7), "TEST LOG MESSAGE 7", 2, 2 ),
//      grokLogMessages += GrokLogMessages( Some(8), "TEST LOG MESSAGE 8", 2, 2 ),
//      grokLogMessages += GrokLogMessages( Some(9), "TEST LOG MESSAGE 9", 2, 2 ),
//      grokLogMessages += GrokLogMessages( Some(10), "TEST LOG MESSAGE 0", 2, 2 )
    )

    try{
      Await.result( db.run( action.transactionally ), Duration.Inf )
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    session.close()
    Response.ok.build()
  }

  @GET
  @Path("create")
  @Produces(Array(APPLICATION_JSON))
  def create(): Response = {
//    val db = Database.forConfig(dbType)

    val session = db.createSession()
    try {
      val userSchema = (
        UserSchema.systems.schema ++
          UserSchema.roles.schema ++
          UserSchema.users.schema ++
          UserSchema.projects.schema ++
          UserSchema.userProjects.schema ++
          UserSchema.projectsDeployed.schema ++
          UserSchema.projectsRejected.schema ++
          UserSchema.notifications.schema ++
          UserSchema.userProjectNotifications.schema ++
          UserSchema.accessTokens.schema ++
          UserSchema.userApplicationProperties.schema
        ).create

      DBIO.seq(

      )
      println("----------------------------------------------")
      userSchema.statements.foreach(println)
      println("----------------------------------------------")
      Await.result(db.run(DBIO.seq(userSchema).transactionally), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    try {
      val logfileSchema = (
        LogFileSchema.logSourceTypes.schema ++
          LogFileSchema.logSources.schema ++
          LogFileSchema.logFiles.schema ++
          LogFileSchema.logLines.schema
        ).create

      println("----------------------------------------------")
      logfileSchema.statements.foreach(println)
      println("----------------------------------------------")
      Await.result(db.run(DBIO.seq(logfileSchema).transactionally), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    try {
      val grokSchema = (
        GrokSchema.groks.schema ++
        GrokSchema.grokLogMessages.schema
      ).create
      println("----------------------------------------------")
      grokSchema.statements.foreach(println)
      println("----------------------------------------------")
      Await.result(db.run(DBIO.seq(grokSchema).transactionally), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    try {
      val llSchema = LogLikelihoodSchema.logLikelihoodRun.schema.create

      println("----------------------------------------------")
      llSchema.statements.foreach(println)
      println("----------------------------------------------")
      Await.result(db.run(DBIO.seq(llSchema).transactionally), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    try {
      val serverSchema = (
        ServerSchema.servers.schema ++
          ServerSchema.labels.schema ++
          ServerSchema.serverLabels.schema
        ).create

      println("----------------------------------------------")
      serverSchema.statements.foreach(println)
      println("----------------------------------------------")
      Await.result(db.run(DBIO.seq(serverSchema).transactionally), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    try {
      val logLineCountsSchema = LogLineCountSchema.logLineCounts.schema.create
      println("----------------------------------------------")
      logLineCountsSchema.statements.foreach(println)
      println("----------------------------------------------")
      Await.result(db.run(DBIO.seq(logLineCountsSchema).transactionally), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }
    session.close()
    Response.ok.build()
  }

  @GET
  @Path("delete")
  @Produces(Array(APPLICATION_JSON))
  def delete(): Response = {
//    val db = Database.forConfig(dbType)
    val session = db.createSession()

    try {
      val deleteServerSchema = ServerSchema.serverLabels.schema.drop andThen
        ServerSchema.servers.schema.drop andThen
        ServerSchema.labels.schema.drop

      Await.result(db.run(DBIO.seq(deleteServerSchema).transactionally), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    try {
      val deleteLLSchema = LogLikelihoodSchema.logLikelihoodRun.schema.drop

      Await.result(db.run(DBIO.seq(deleteLLSchema).transactionally), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    try {
      val logLineCountsSchema = LogLineCountSchema.logLineCounts.schema.drop
      Await.result(db.run(DBIO.seq(logLineCountsSchema).transactionally), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    try {
      val deleteGrokSchema = GrokSchema.grokLogMessages.schema.drop andThen
      GrokSchema.grokGrokConfigs.schema.drop andThen
        GrokSchema.groks.schema.drop andThen
      GrokSchema.grokConfigs.schema.drop
      Await.result(db.run(DBIO.seq(deleteGrokSchema).transactionally), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    try {
      val deleteLogSchema = LogFileSchema.logSources.schema.drop andThen
        LogFileSchema.logSourceTypes.schema.drop andThen
        LogFileSchema.logLines.schema.drop andThen
        LogFileSchema.logFiles.schema.drop
      Await.result(db.run(DBIO.seq(deleteLogSchema).transactionally), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    try {
      val deleteUserSchema = UserSchema.userProjects.schema.drop andThen
        UserSchema.projectsDeployed.schema.drop andThen
        UserSchema.projectsRejected.schema.drop andThen
        UserSchema.userProjectNotifications.schema.drop andThen
        UserSchema.notifications.schema.drop andThen
        UserSchema.accessTokens.schema.drop andThen
        UserSchema.userApplicationProperties.schema.drop andThen
        UserSchema.projects.schema.drop andThen
        UserSchema.users.schema.drop andThen
        UserSchema.roles.schema.drop andThen
        UserSchema.systems.schema.drop

      Await.result(db.run(DBIO.seq(deleteUserSchema).transactionally), Duration.Inf)
    } catch {
      case ex: Exception => ex.printStackTrace()
    }

    session.close()
    Response.ok.build()
  }

}   
