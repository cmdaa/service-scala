package io.cmdaa.service.resource.websocket

import com.typesafe.scalalogging.Logger
import org.atmosphere.config.service.{ManagedService, Message, Ready, Singleton}
import org.atmosphere.cpr.{AtmosphereResource, Broadcaster}

@Singleton
@ManagedService(path = "/ws/cast")
class CasterService {

  val logger: Logger = Logger[CasterService]

  @Ready
  def onReady(resource: AtmosphereResource): Unit = {
    Caster.broadcaster = Some(resource.getBroadcaster)
    logger.info(s"Browser ${resource.uuid()} connected")
    logger.info(s"Broadcaster ID ${resource.getBroadcaster.getID}")
  }

  @Message
  def onMessage(message: String ): String = {
    message
  }
}

object Caster {

  var broadcaster: Option[Broadcaster] = None

  def broadcast(message: String) = {
    if (broadcaster.nonEmpty) {
      broadcaster.get.broadcast(message)
    }
  }
}
