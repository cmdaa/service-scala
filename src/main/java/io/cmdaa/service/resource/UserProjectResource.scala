package io.cmdaa.service.resource

import java.util.Calendar

import com.google.gson.{Gson, GsonBuilder}
import io.cmdaa.data.dao.UserProjectRepo
import io.cmdaa.data.util.entity.UserSchema.{Notification, Project, ProjectDeployed, ProjectRejected, Role, System, User, UserApplicationProperty, UserProjectNotification}
import io.cmdaa.data.util.OptionSerializer
import io.cmdaa.service.auth.AppUser
import io.cmdaa.service.notification.mail.{DeadLetterOffice, Message}
import io.cmdaa.service.resource.websocket.Caster
import io.cmdaa.service.util.PasswordStorage
import io.dropwizard.auth.Auth
import javax.annotation.security.RolesAllowed
import javax.ws.rs._
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response

import scala.collection.mutable.ListBuffer

@Path("/")
class UserProjectResource(userProjectRepo: UserProjectRepo) extends AbstractResource(userProjectRepo: UserProjectRepo) {

  val gsonb: GsonBuilder = new GsonBuilder()
  gsonb.registerTypeAdapter(classOf[Option[Any]], new OptionSerializer)
  val gson: Gson = gsonb.create

  @POST
  @Path("user")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertUser(@Auth principal: AppUser, in: Map[String, Any]): Response = {

    // Create password hash
    val pwordHash: String = PasswordStorage.createHash(in("password").toString)

    val user: User = userProjectRepo.insertUser(User(
      None,
      in.getOrElse("username", "").toString,
      pwordHash,
      in.getOrElse("firstName", "").toString,
      in.getOrElse("lastName", "").toString,
      in.getOrElse("email", "").toString,
      in.getOrElse("active", true).asInstanceOf[Boolean],
      in.getOrElse("approved", false).asInstanceOf[Boolean],
      who(principal),
      Calendar.getInstance().getTime.getTime
    ))

    userProjectRepo.insertNotification(Notification(None, s"Inserted new user: ${user.username}", 1, Calendar.getInstance().getTime.getTime), List((user.id, None), (Some(principal.id), None)))

    DeadLetterOffice.dispatch(new Message(List(user.email), "admin@foo.com", "Admin Inserting a User", s"Inserted new user: ${user.username}"))

    Caster.broadcast("ALERT")

    Response.ok.entity(user).build()
  }

  @POST
  @Path("user/new")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertNewUser(in: Map[String, Any]): Response = {

    // Create password hash
    val pwordHash: String = PasswordStorage.createHash(in("password").toString)
    val username: String = in.getOrElse("username", "").toString

    val user: User = userProjectRepo.insertUser(User(
      None,
      username,
      pwordHash,
      in.getOrElse("firstName", "").toString,
      in.getOrElse("lastName", "").toString,
      in.getOrElse("email", "").toString,
      in.getOrElse("active", true).asInstanceOf[Boolean],
      in.getOrElse("approved", false).asInstanceOf[Boolean],
      username,
      Calendar.getInstance().getTime.getTime
    ))

    userProjectRepo.insertNotification(Notification(None, s"Inserted new user: ${user.username}", 1, Calendar.getInstance().getTime.getTime), List((user.id, None)))

    DeadLetterOffice.dispatch(new Message(List(user.email), "admin@foo.com", "User Creating a New User", s"Inserted new user: ${user.username}"))

    Caster.broadcast("ALERT")

    Response.ok.entity(user).build()
  }

  @PUT
  @Path("user")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def updateUser(@Auth principal: AppUser, in: Map[String, Any]): Response = {

    val user: User = User(
      Some(in("id").asInstanceOf[Int].asInstanceOf[Long]),
      in.getOrElse("username", "").toString,
      in.getOrElse("password", "").toString,
      in.getOrElse("firstName", "").toString,
      in.getOrElse("lastName", "").toString,
      in.getOrElse("email", "").toString,
      in.getOrElse("active", true).asInstanceOf[Boolean],
      in.getOrElse("approved", false).asInstanceOf[Boolean],
      who(principal),
      Calendar.getInstance().getTime.getTime
    )

    Response.ok.entity(userProjectRepo.updateUser(user)).build()
  }

  @DELETE
  @Path("user/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteUser(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.deleteUser(id)).build()
  }

  @GET
  @Path("user/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findUser(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    val user: Option[User] = userProjectRepo.findUser(id)
    if (user.isEmpty) {
      Response.status(Response.Status.NOT_FOUND).build()
    } else {
      Response.ok.entity(user).build()
    }
  }

  @GET
  @Path("user")
  @Produces(Array(APPLICATION_JSON))
  def listUsers(@Auth principal: AppUser): Response = {
    Response.ok.entity(userProjectRepo.listUsers()).build()
  }

  @PUT
  @Path("user/{id}/password")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def updatePassword(@Auth principal: AppUser, @PathParam("id") id: Long, password: String): Response = {

    // Create password hash
    val pwordHash: String = PasswordStorage.createHash(password)

    Response.ok.entity(userProjectRepo.updatePassword(id, pwordHash)).build()
  }

  @POST
  @Path("role")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertRole(@Auth principal: AppUser, in: Map[String, Any]): Response = {

    val role: Role = Role(
      None,
      in.getOrElse("name", "").toString,
      in.getOrElse("description", "").toString,
      who(principal),
      Calendar.getInstance().getTime.getTime
    )
    Response.ok.entity(userProjectRepo.insertRole(role)).build()
  }

  @PUT
  @Path("role")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def updateRole(@Auth principal: AppUser, in: Map[String, Any]): Response = {

    val role: Role = Role(
      Some(in("id").asInstanceOf[Int].asInstanceOf[Long]),
      in.getOrElse("name", "").toString,
      in.getOrElse("description", "").toString,
      who(principal),
      Calendar.getInstance().getTime.getTime
    )
    Response.ok.entity(userProjectRepo.updateRole(role)).build()
  }

  @DELETE
  @Path("role/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteRole(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.deleteRole(id)).build()
  }

  @GET
  @Path("role/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findRole(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    val role: Option[Role] = userProjectRepo.findRole(id)
    if (role.isEmpty) {
      Response.status(Response.Status.NOT_FOUND).build()
    } else {
      Response.ok.entity(role).build()
    }
  }

  @GET
  @Path("role")
  @Produces(Array(APPLICATION_JSON))
  def listRoles(@Auth principal: AppUser): Response = {
    Response.ok.entity(userProjectRepo.listRoles()).build()
  }

  @POST
  @Path("system")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertSystem(@Auth principal: AppUser, in: Map[String, Any]): Response = {

    val system: System = System(
      None,
      in.getOrElse("name", "").toString,
      in.getOrElse("description", "").toString,
      who(principal),
      Calendar.getInstance().getTime.getTime
    )
    Response.ok.entity(userProjectRepo.insertSystem(system)).build()
  }

  @PUT
  @Path("system")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def updateSystem(@Auth principal: AppUser, in: Map[String, Any]): Response = {

    val system: System = System(
      Some(in("id").asInstanceOf[Int].asInstanceOf[Long]),
      in.getOrElse("name", "").toString,
      in.getOrElse("description", "").toString,
      who(principal),
      Calendar.getInstance().getTime.getTime
    )
    Response.ok.entity(userProjectRepo.updateSystem(system)).build()
  }

  @DELETE
  @Path("system/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteSystem(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.deleteSystem(id)).build()
  }

  @GET
  @Path("system/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findSystem(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    val system: Option[System] = userProjectRepo.findSystem(id)
    if (system.isEmpty) {
      Response.status(Response.Status.NOT_FOUND).build()
    } else {
      Response.ok.entity(system).build()
    }
  }

  @GET
  @Path("system")
  @Produces(Array(APPLICATION_JSON))
  def listSystems(@Auth principal: AppUser): Response = {
    Response.ok.entity(userProjectRepo.listSystems()).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @GET
  @Path("project")
  @Produces(Array(APPLICATION_JSON))
  def listProjects(@Auth principal: AppUser): Response = {
    Response.ok.entity(userProjectRepo.listProjects()).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @GET
  @Path("project/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findProject(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    val project: Option[Project] = userProjectRepo.findProject(id)
    if (project.isEmpty) {
      Response.status(Response.Status.NOT_FOUND).build()
    } else {
      Response.ok.entity(project).build()
    }
  }

  @RolesAllowed(Array("ADMIN"))
  @GET
  @Path("project/{id}/logSource")
  @Produces(Array(APPLICATION_JSON))
  def listLogSourcesForProject(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.listLogSourcesForProject(id)).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @GET
  @Path("project/{id}/user")
  @Produces(Array(APPLICATION_JSON))
  def listUsersForProject(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.listUsersForProject(id)).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @GET
  @Path("project/{id}/userRole")
  @Produces(Array(APPLICATION_JSON))
  def listUserRolesForProject(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.listUserRolesForProject(id)).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @GET
  @Path("project/{id}/system")
  @Produces(Array(APPLICATION_JSON))
  def getSystemForProject(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.getSystemForProject(id)).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @GET
  @Path("project/{id}/deploy")
  @Produces(Array(APPLICATION_JSON))
  def listDeploymentsForProject(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.listDeploymentsForProject(id)).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @POST
  @Path("project/{id}/deploy")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def deployProject(@Auth principal: AppUser, @PathParam("id") id: Long, in: Map[String, Any]): Response = {
    val deployment: ProjectDeployed = ProjectDeployed(
      id,
      in.getOrElse("deployType", "TEST").toString,
      who(principal),
      Calendar.getInstance().getTime.getTime
    )
    Response.ok.entity(userProjectRepo.deployProject(deployment)).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @GET
  @Path("project/{id}/reject")
  @Produces(Array(APPLICATION_JSON))
  def listRejectionsForProject(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.listRejectionsForProject(id)).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @POST
  @Path("project/{id}/reject")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def rejectProject(@Auth principal: AppUser, @PathParam("id") id: Long, in: Map[String, Any]): Response = {
    val rejection: ProjectRejected = ProjectRejected(
      id,
      in("rejectReason").toString,
      who(principal),
      Calendar.getInstance().getTime.getTime
    )
    Response.ok.entity(userProjectRepo.rejectProject(rejection)).build()
  }

  @GET
  @Path("project/me")
  @Produces(Array(APPLICATION_JSON))
  def listMyProjects(@Auth principal: AppUser): Response = {
    Response.ok.entity(userProjectRepo.listProjectsForUser(principal.id)).build()
  }

  @GET
  @Path("project/me/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findMyProject(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.findProjectForUser(id, principal.id)).build()
  }


  @GET
  @Path("project/{id}/owner")
  @Produces(Array(APPLICATION_JSON))
  def getOwnerForProject(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.getOwnerForProject(id)).build()
  }

  @GET
  @Path("project/owned")
  @Produces(Array(APPLICATION_JSON))
  def listMyOwnedProjects(@Auth principal: AppUser): Response = {
    Response.ok.entity(userProjectRepo.listProjectsForOwner(principal.id)).build()
  }

  @GET
  @Path("project/owned/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findMyOwnedProject(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.findProjectForOwner(id, principal.id)).build()
  }

  @POST
  @Path("project/owned")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertMyProject(@Auth principal: AppUser, in: Map[String, Any]): Response = {

    val system: Map[String, Any] = in("system").asInstanceOf[Map[String, Any]]
    val systemId: Long = system("id").asInstanceOf[Number].longValue()
    val regExQuantity: Option[Long] = try {
      Some(in("regExQuantity").toString.toLong)
    } catch {
      case _: Exception => None
    }
    val regExUnit: Option[String] = try {
      Some(in("regExUnit").asInstanceOf[String])
    } catch {
      case _: Exception => None
    }

    val project: Project = Project(
      None,
      in.getOrElse("name", "").toString,
      systemId,
      principal.id,
      in.getOrElse("suggestRegEx", false).asInstanceOf[Boolean],
      regExQuantity,
      regExUnit,
      "PENDING",
      who(principal),
      Calendar.getInstance().getTime.getTime
    )

    val logSources = ListBuffer[(Option[Long], String, Long, Option[Long], String, Option[Long], String, Long)]()
    val logSourceMaps = in.getOrElse("logSources", List.empty[Map[String, Any]]).asInstanceOf[List[Map[String, Any]]]

    for (logSourceMap <- logSourceMaps) {
      val logSourceType: Map[String, Any] = logSourceMap("type").asInstanceOf[Map[String, Any]]
      val logSourceTypeId: Long = logSourceType("id").asInstanceOf[Number].longValue()
      val notifyRowQty: Option[Long] = try {
        Some(logSourceMap("notifyRowQty").toString.toLong)
      } catch {
        case _: Exception => None
      }
      logSources += ((
        None,
        logSourceMap.getOrElse("name", "").toString,
        logSourceTypeId,
        notifyRowQty,
        logSourceMap.getOrElse("meta", "").toString,
        project.id,
        who(principal),
        Calendar.getInstance().getTime.getTime
      ))
    }

    val userRoles = new ListBuffer[(Long, Long)]()
    val userRoleMaps = in.getOrElse("userRoles", List.empty[Map[String, Any]]).asInstanceOf[List[Map[String, Any]]]

    for (userRoleMap <- userRoleMaps) {
      val userMap = userRoleMap("user").asInstanceOf[Map[String, Any]]
      val roleMap = userRoleMap("role").asInstanceOf[Map[String, Any]]
      val userId: Long = userMap("id").asInstanceOf[Int].asInstanceOf[Long]
      val roleId: Long = roleMap("id").asInstanceOf[Int].asInstanceOf[Long]
      userRoles += ((userId, roleId))
    }

    userProjectRepo.insertNotification(Notification(None, s"Inserted new project: ${project.name}", 1, Calendar.getInstance().getTime.getTime), List((Some(principal.id), None)))

    Caster.broadcast("ALERT")

    Response.ok.entity(userProjectRepo.insertProject(project, logSources.toList, userRoles.toList)).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @GET
  @Path("notification/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findNotification(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    val notification: Option[Notification] = userProjectRepo.findNotification(id)
    if (notification.isEmpty) {
      Response.status(Response.Status.NOT_FOUND).build()
    } else {
      Response.ok.entity(notification).build()
    }
  }

  @RolesAllowed(Array("ADMIN"))
  @GET
  @Path("notification")
  @Produces(Array(APPLICATION_JSON))
  def listNotifications(@Auth principal: AppUser, @QueryParam("offset") offset: Long, @QueryParam("limit") limit: Long): Response = {
    case class Resp(count: Long, notifications: List[Notification])
    var pagination: Option[(Long, Long)] = None
    if (limit > 0) {
      pagination = Some(offset, limit)
    }
    val res: (Int, List[Notification]) = userProjectRepo.listNotifications(pagination)
    Response.ok.entity(Resp(res._1, res._2)).build
  }

  @GET
  @Path("notification/me")
  @Produces(Array(APPLICATION_JSON))
  def listMyNotifications(@Auth principal: AppUser, @QueryParam("offset") offset: Long, @QueryParam("limit") limit: Long): Response = {
    case class Resp(count: Long, notifications: List[(Notification, UserProjectNotification)])
    var pagination: Option[(Long, Long)] = None
    if (limit > 0) {
      pagination = Some(offset, limit)
    }
    val res: (Int, List[(Notification, UserProjectNotification)]) = userProjectRepo.listNotificationsForUser(principal.id, pagination)
    Response.ok.entity(Resp(res._1, res._2)).build
  }

  @GET
  @Path("notification/me/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findMyNotification(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(userProjectRepo.findNotificationForUser(id, principal.id)).build()
  }

  @PUT
  @Path("notification/me/{id}")
  @Produces(Array(APPLICATION_JSON))
  def ackMyNotification(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Caster.broadcast("ALERT")
    Response.ok.entity(userProjectRepo.acknowledgeNotification(principal.id, id, Calendar.getInstance().getTime.getTime)).build()
  }

  @GET
  @Path("notification/me/unack/count")
  @Produces(Array(APPLICATION_JSON))
  def countMyUnackNotifications(@Auth principal: AppUser): Response = {
    Response.ok.entity(userProjectRepo.countUnacknowledgedNotificationsForUser(principal.id)).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @GET
  @Path("userAppProperty")
  @Produces(Array(APPLICATION_JSON))
  def listUserApplicationProperties(@Auth principal: AppUser): Response = {
    Response.ok.entity(userProjectRepo.listUserApplicationProperties()).build()
  }

  @GET
  @Path("userAppProperty/me")
  @Produces(Array(APPLICATION_JSON))
  def listMyApplicationProperties(@Auth principal: AppUser): Response = {
    Response.ok.entity(userProjectRepo.listUserApplicationPropertiesForUser(principal.id)).build()
  }

  @GET
  @Path("userAppProperty/me/{key}")
  @Produces(Array(APPLICATION_JSON))
  def findMyApplicationProperty(@Auth principal: AppUser, @PathParam("key") key: String): Response = {
    Response.ok.entity(userProjectRepo.findUserApplicationProperty(principal.id, key)).build()
  }

  @POST
  @Path("userAppProperty/me")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertOrUpdateMyApplicationProperty(@Auth principal: AppUser, in: Map[String, Any]): Response = {

    val key: String = in("key").toString

    val prop: UserApplicationProperty = UserApplicationProperty(
      principal.id,
      key,
      in("value").toString
    )

    if (userProjectRepo.findUserApplicationProperty(principal.id, key).isEmpty) {
      Response.ok.entity(userProjectRepo.insertUserApplicationProperty(prop)).build()
    } else {
      Response.ok.entity(userProjectRepo.updateUserApplicationProperty(prop)).build()
    }
  }

  @DELETE
  @Path("userAppProperty/me/{key}")
  @Produces(Array(APPLICATION_JSON))
  def deleteMyApplicationProperty(@Auth principal: AppUser, @PathParam("key") key: String): Response = {
    Response.ok.entity(userProjectRepo.deleteUserApplicationProperty(principal.id, key)).build()
  }

  def findAllProjects(): Response = {
    Response.ok.entity(userProjectRepo.findAllProjects()).build()
  }

  def findAllUserProjects(id: Long): Response = {
    Response.ok.entity(userProjectRepo.findAllUserProjects()).build()
  }

  def addUserToProject(userId: Long, projectId: Long, userTypeId: Long): Response = {
    Response.ok.entity(userProjectRepo.addUserToProject(userId, projectId, userTypeId)).build()
  }

  //  def addUserToProject(user: User, project: Project, userType: Role)

  def findProjectsByUser(userId: Long): Response = {
    Response.ok.entity(userProjectRepo.findProjectsByUser(userId)).build()
  }

  def findUsersInProject(projectId: Long): Response = {
    Response.ok.entity(userProjectRepo.findUsersInProject(projectId)).build()
  }

  def getListOfUsersWithUserType(projectId: Long): Response = {
    Response.ok.entity(userProjectRepo.getListOfUsersWithUserType(projectId)).build()
  }

}