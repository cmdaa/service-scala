package io.cmdaa.service

import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.Configuration
import javax.validation.Valid

class CmdaaAppConfig extends Configuration {
  @Valid
  @JsonProperty
  var allowedGrantTypes: Array[String] = _

  @Valid
  @JsonProperty
  var accessTokenExpireTimeMillis: Long = 30L * 60L * 1000L // 30 Minutes
}
