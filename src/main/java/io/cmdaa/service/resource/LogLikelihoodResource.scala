package io.cmdaa.service.resource

import com.google.gson.Gson
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response
import javax.ws.rs.{GET, Path, PathParam, Produces}
import io.cmdaa.data.dao.LogLikelihoodAnalyticRepo
import io.cmdaa.data.util.entity.LogLikelihoodSchema.LogLikeLihoodRun

@Path("/log-likelihood")
class LogLikelihoodResource(loglikelihoodRepo: LogLikelihoodAnalyticRepo) {

  val gson = new Gson()

  @GET
  @Path("find-all-runs-by-log-file/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findAllRunsByLogFile(@PathParam("id") id: Long): Response = {
    Response.ok.entity(loglikelihoodRepo.findAllRunsByLogFile(id)).build()
  }

  @GET
  @Path("delete-by-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteById(@PathParam("id") id: Long): Response = {
    Response.ok.entity(loglikelihoodRepo.deleteById(id)).build()
  }

  @GET
  @Path("delete-by-log-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteByLogId(@PathParam("id") logId: Long): Response = {
    Response.ok.entity(loglikelihoodRepo.deleteByLogId(logId)).build()
  }

  @GET
  @Path("find-all")
  @Produces(Array(APPLICATION_JSON))
  def findAll(): Response = {
    Response.ok.entity(loglikelihoodRepo.findAll()).build()
  }

  @GET
  @Path("find-all-with-log-file")
  @Produces(Array(APPLICATION_JSON))
  def findAllWithLogFile(): Response = {
    Response.ok.entity(loglikelihoodRepo.findAllWithLogFile()).build()
  }

  @GET
  @Path("insert/{list}/{logLikeLikelihoodRun}")
  @Produces(Array(APPLICATION_JSON))
  def insert(@PathParam("list") list: Boolean, @PathParam("logLikeLikelihoodRun") logLikeLihoodRun: String): Response = {
    if (list) {
      Response.ok.entity(loglikelihoodRepo.insert(gson.fromJson(logLikeLihoodRun, classOf[List[LogLikeLihoodRun]]))).build()
    } else {
      Response.ok.entity(loglikelihoodRepo.insert(gson.fromJson(logLikeLihoodRun, classOf[LogLikeLihoodRun]))).build()
    }
  }
}