package io.cmdaa.service.auth

import java.security.Principal

class AppUser(
                var id: Long,
                var name: String,
                var roles: Set[String]
              ) extends Principal {

  override def getName: String = { name }
}
