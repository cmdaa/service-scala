package io.cmdaa.service.resource

import com.google.gson._
import org.junit.runner.RunWith
import io.cmdaa.data.dao.LogLineRepo
import io.cmdaa.data.util.entity.LogFileSchema.LogLine
import io.cmdaa.data.util.OptionSerializer
import org.scalamock.scalatest.MockFactory
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}

@RunWith(classOf[JUnitRunner])
class LogLineResourceTest  extends FlatSpec with MockFactory with BeforeAndAfter {
  val logLine = LogLine(Some(1), "This is a test log line.", 1, retain = false)
  val listLogLine = List( LogLine(Some(1), "This is a test log line.", 1, retain = false), LogLine(Some(2), "This is a test log line.", 1, retain = false) )

  val logLineRepo: LogLineRepo = stub[LogLineRepo]
  val logLineResource = new LogLineResource(logLineRepo)

  val gsonb = new GsonBuilder()
  gsonb.registerTypeAdapter(classOf[Option[Any]], new OptionSerializer)
  val gson: Gson = gsonb.create

  "findById" should "should return a line" in {
    (logLineRepo.findById _).when(1).returns(Some(logLine))
    assert(logLineResource.findById(1).getEntity === Some(logLine))
  }

  "findAllLinesByLogFile" should "" in {
    (logLineRepo.findAllLinesByLogFile _).when(1).returning(listLogLine.toStream)
    assert(logLineResource.findAllLinesByLogFile(1).getEntity === listLogLine)
  }

  "deleteByLogId" should "" in {
    (logLineRepo.deleteByLogId _).when(1).returns(12)
    assert(logLineResource.deleteByLogId(1).getEntity === 12)
  }

  "findAll" should "" in {
    (logLineRepo.findAll _).when().returns(listLogLine.toStream)
    assert(logLineResource.findAll().getEntity === listLogLine)
  }

  "insert logline" should "" in {
    val f = gson.toJson(logLine)
    println(f)

    val b = gson.fromJson(f, classOf[LogLine])
    println(b)

  }

  "insert list" should "" in {
    (logLineRepo.insert(_: Stream[LogLine])).when(listLogLine.toStream).returns(())
  }

  "markRecordAsRetain" should "" in {

  }
}
