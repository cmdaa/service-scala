package io.cmdaa.service.resource

import com.google.gson.GsonBuilder
import io.cmdaa.data.dao.LogLineCountRepo
import io.cmdaa.data.util.entity.LogLineCountSchema._
import io.cmdaa.data.util.OptionSerializer
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response
import javax.ws.rs._

@Path("/log-line-count")
class LogLineCountResource(logLineCountRepo: LogLineCountRepo) {
  val gsonb = new GsonBuilder()
  gsonb.registerTypeAdapter(classOf[Option[Any]], new OptionSerializer)
  val gson = gsonb.create


  @GET
  @Path("find-all-lines-by-log-file/{logFileId}")
  @Produces(Array(APPLICATION_JSON))
  def findAllLinesByLogFile(@PathParam("logFileId") logFileId: Long): Response = {
    Response.ok.entity(logLineCountRepo.findAllLinesByLogFile(logFileId).toList).build()
  }

  @GET
  @Path("delete-by-log-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteByLogId(@PathParam("id") id: Long): Response = {
    Response.ok.entity(logLineCountRepo.deleteByLogId(id)).build()
  }

  @POST
  @Path("insert/{list}/{logLineCount}")
  @Produces(Array(APPLICATION_JSON))
  def insert(@PathParam("list") list: Boolean, @PathParam("logLineCount") logLineCount: String): Response = {
    if (list) {
      /* debug code
      println("Printing logLineCount Stream data before insert")
      println(logLineCount)
      */

      //parse the string input from JSON format to create an Array of LogLineCount elements
      //I copy to an Array of LogLineCount instead of to a Stream of LogLineCount because the gson.fromJson
      //method fails when I try that...thinking that might be a Java to Scala conversion issue.
      val logLineCountArr = gson.fromJson(logLineCount, classOf[Array[LogLineCount]])

      //Create the Stream of LogLineCount from the String array....don't try to combine the line above
      //with this line.   The compiler will choke.  I tried
      val newStream: Stream[LogLineCount] = Stream.empty ++ logLineCountArr

      Response.ok.entity(logLineCountRepo.insert(newStream)).build()
    } else {

      /* debug code
      println("Printing logLineCount String data before insert")
      println(logLineCount)
      */
      val v = gson.fromJson(logLineCount, classOf[LogLineCount])

      /* debug code
      println( v )
      */

      Response.ok.entity(logLineCountRepo.insert(gson.fromJson(logLineCount, classOf[LogLineCount]))).build()
    }
  }

  @POST
  @Path("update-transaction/{statementList}")
  @Produces(Array(APPLICATION_JSON))
  def updateTransaction(@PathParam("statementList") statementList: String): Response = {

    /* debug code
    println("input statement is: ")
    println(statementList)
    */
    // grab the string and interpret it as a JSON and parse it into an array of LineCountUpdate
    val updateValueArr = gson.fromJson(statementList, classOf[Array[LineCountUpdate]])
    /* debug code
    println("parsed values are:")
    println(s"id is ${updateValueArr(0).logId} md5 is ${updateValueArr(1).md5Id}")
    */
    //Convert the updateValueArr of Strings to a Seq of Strings
    val updateValueSeq: Seq[LineCountUpdate] = Seq.empty ++: updateValueArr

    //Apply the values in the updateValueSeq to an update statement with string interpolation
    //Now we have the sequence of SQL statements
    //insert into LOG_LINE_COUNT (log_id,log_line_id,md5_id,count) VALUES (1,1,'FAKE-MD5-5',20) ON DUPLICATE KEY UPDATE COUNT = COUNT + 20;
    val statementSeq: Seq[String] = updateValueSeq.map(
      //    x => s"update LOG_LINE_COUNT SET COUNT = COUNT + ${x.count} where LOG_ID = ${x.logId} AND MD5_ID = '${x.md5Id}'"
      x => s"insert into LOG_LINE_COUNT (log_id,md5_id,count) VALUES (${x.logId},'${x.md5Id}',${x.count}) ON DUPLICATE KEY UPDATE COUNT = COUNT + ${x.count}"

    )
    /* debug code
    println("update statements are:")
    statementSeq.foreach(println(_))
    */

    Response.ok.entity(logLineCountRepo.sendTransaction(statementSeq)).build()
  }


}
