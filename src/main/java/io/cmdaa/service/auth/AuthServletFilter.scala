package io.cmdaa.service.auth

import scala.collection.JavaConverters._

import java.security.cert.X509Certificate

import javax.naming.ldap.{LdapName, Rdn}
import javax.servlet.http.HttpServletRequest
import javax.servlet.{ServletRequest, _}

class AuthServletFilter extends Filter {
  override def init(filterConfig: FilterConfig): Unit = {}

  override def doFilter(servletRequest: ServletRequest, servletResponse: ServletResponse, filterChain: FilterChain): Unit = {
    val request: HttpServletRequest = servletRequest.asInstanceOf[HttpServletRequest]
    val opt = Option(request.getAttribute("javax.servlet.request.X509Certificate"))
    if (opt.nonEmpty) {
      println("====> Found something in certs")
      val certs: Array[X509Certificate] = opt.get.asInstanceOf[Array[X509Certificate]]
      for (cert <- certs) {
        val dn: String = cert.getSubjectDN.getName
        println(s"====> DN: $dn")
        val ldapDN: LdapName = new LdapName(dn)
        for(rdn: Rdn <- ldapDN.getRdns.asScala.toSet) {
          if ("CN" == rdn.getType) {
            println(s"====> Type: ${rdn.getType}, Value: ${rdn.getValue}")
          }
        }
      }
    }
    filterChain.doFilter(servletRequest, servletResponse)
  }

  override def destroy(): Unit = {}
}
