package io.cmdaa.service.resource

import com.google.gson._
import org.junit.runner.RunWith
import io.cmdaa.data.dao.LogLineCountRepo
import io.cmdaa.data.util.entity.LogLineCountSchema._
import io.cmdaa.data.util.OptionSerializer
import org.scalamock.scalatest.MockFactory
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}

@RunWith(classOf[JUnitRunner])
class LogLineCountResourceTest  extends FlatSpec with MockFactory with BeforeAndAfter {
  val logLineCounts = LogLineCount(Some(1), 1, "TEST MD5-VALUE-1", 5)
  val listLogLineCount = List( LogLineCount(Some(2), 2, "TEST MD5-VALUE-2", 10), LogLineCount(Some(3), 3, "TEST MD5-VALUE-3", 15) )

  val logLineCountRepo = stub[LogLineCountRepo]
  val logLineCountResource = new LogLineCountResource(logLineCountRepo)

  val gsonb = new GsonBuilder()
  gsonb.registerTypeAdapter(classOf[Option[Any]], new OptionSerializer)
  val gson = gsonb.create



  "findAllLinesByLogFile" should "" in {
    (logLineCountRepo.findAllLinesByLogFile _).when(1).returning(listLogLineCount.toStream)
    assert(logLineCountResource.findAllLinesByLogFile(1).getEntity == listLogLineCount)
  }


  "deleteByLogId" should "" in {
    (logLineCountRepo.deleteByLogId _).when(1).returns(12)
    assert(logLineCountResource.deleteByLogId(1).getEntity == 12)
  }

  "insert logLineCount" should "" in {
    val f = gson.toJson(logLineCounts)
    println(f)

    val b = gson.fromJson(f, classOf[LogLineCount])
    println(b)

  }

  "insert list" should "" in {
    (logLineCountRepo.insert(_: Stream[LogLineCount])).when(listLogLineCount.toStream).returns(())
  }

}
