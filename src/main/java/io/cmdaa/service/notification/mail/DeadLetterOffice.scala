package io.cmdaa.service.notification.mail

import com.typesafe.scalalogging.Logger
import io.cmdaa.service.notification.Dispatcher

object DeadLetterOffice extends Dispatcher[Message] {

  val logger: Logger = Logger("DeadLetterOffice")

  def send(message: Message): Unit = {
    logger.info("Sending the following message ====>")
    for (to <- message.to) { logger.info(s"=====> To: $to") }
    logger.info(s"=====> From: ${message.from}")
    logger.info(s"=====> Subject: ${message.subject}")
    logger.info(s"=====> Content: ${message.content}")
  }

  override def dispatch(payload: Message): Unit = {
    send(payload)
  }
}
