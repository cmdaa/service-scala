package io.cmdaa.service.resource

import com.google.gson.{Gson, GsonBuilder}
import org.junit.runner.RunWith
import io.cmdaa.data.dao.{ServerRepo, UserProjectRepo}
import io.cmdaa.data.util.entity.ServerSchema.{Label, Server}
import io.cmdaa.data.util.OptionSerializer
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfter, FlatSpec}
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ServerResourceTest extends FlatSpec with MockFactory with BeforeAndAfter {
  val server = Server(Some(1L), "foo.com", "127.0.0.1", "::1/128", "modifier", 10000)
  val serverLabelHttpd = Label(Some(1), "httpd", "modifier", 10000)
  val serverLabelNginx = Label(Some(2), "nginx", "modifier", 10000)

  val serverRepo: ServerRepo = stub[ServerRepo]
  val userProjectRepo: UserProjectRepo = stub[UserProjectRepo]

  val serverResource = new ServerResource(serverRepo, userProjectRepo)
  val gsonb = new GsonBuilder()
  gsonb.registerTypeAdapter(classOf[Option[Any]], new OptionSerializer)
  val gson: Gson = gsonb.create

  "findAllServers" should "return list of servers" in {
    (serverRepo.listServers _).when().returns(List(server))
    assert(serverResource.findAllServers().getEntity == List(server))
  }

  "findServerById" should "find server by id" in {
    (serverRepo.findServer _).when(1).returns(Some(server))
    assert(serverResource.findServerById(1).getEntity == Some(server))
  }

  "deleteServerById" should "delete a single server" in {
    (serverRepo.deleteServer _).when(1).returns(1)
    assert(serverResource.deleteServerById(1).getEntity === 1)
  }

  "findAllServersLabels" should "return all server labels" in {
    (serverRepo.listLabels _).when().returns(List(serverLabelHttpd, serverLabelNginx))
    assert(serverResource.findAllServersLabels().getEntity == List(serverLabelHttpd, serverLabelNginx))
  }

  "findServerLabelById" should "find server label" in {
    (serverRepo.findLabel _).when(1).returns(Some(serverLabelHttpd))
    assert(serverResource.findServerLabelById(1).getEntity == Some(serverLabelHttpd))
  }

  "deleteByServerLabelId" should "delete server label" in {
    (serverRepo.deleteLabel _).when(1).returns(1)
    assert(serverResource.deleteByServerLabelId(1).getEntity === 1)
  }

  "getServersByLabel" should "get list of all server labels" in {
    (serverRepo.findServersByLabel _).when(1).returns(List(server, server))
    assert(serverResource.getServersByLabel(1).getEntity == List(server, server))
  }

  "getLabelsForServer" should "get all labels for server" in {
    (serverRepo.findLabelsByServer _).when(1).returns(List(serverLabelHttpd, serverLabelNginx))
    assert(serverResource.getLabelsForServer(1).getEntity == List(serverLabelHttpd, serverLabelNginx))
  }

  "insertServer" should "add server" in {
    (serverRepo.insertServer( _: Server)).when(server).returns(server)
    assert(serverResource.insertServer(gson.toJson(server)).getEntity === server)
  }

  "insertServerLabel" should "add server label" in {
    (serverRepo.insertLabel _).when(serverLabelHttpd).returns(serverLabelHttpd)
    assert(serverResource.insertLabel(gson.toJson(serverLabelHttpd)).getEntity === serverLabelHttpd)
  }

}
