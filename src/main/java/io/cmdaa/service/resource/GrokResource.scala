package io.cmdaa.service.resource

import com.google.gson.{Gson, GsonBuilder}
import javax.ws.rs._
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response
import io.cmdaa.data.dao.{GrokRepo, UserProjectRepo}
import io.cmdaa.data.util.entity.GrokSchema.{Grok, GrokLogMessages}
import io.cmdaa.data.util.OptionSerializer

@Path("/grok")
class GrokResource(grokRepo: GrokRepo, userProjectRepo: UserProjectRepo) extends AbstractResource(userProjectRepo: UserProjectRepo) {

  val gsonb = new GsonBuilder()
  gsonb.registerTypeAdapter(classOf[Option[Any]], new OptionSerializer)
  val gson: Gson = gsonb.create

  @GET
  @Path("find-all-groks")
  @Produces(Array(APPLICATION_JSON))
  def findAllGroks(): Response = {
    Response.ok.entity(grokRepo.findAllGroks()).build()
  }

  @GET
  @Path("find-grok-by-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findGrokById(@PathParam("id") id: Long): Response = {
    Response.ok.entity(grokRepo.findGrokById(id).getOrElse("")).build()
  }

  @POST
  @Path("insert-grok")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertGrok(grok: Grok): Response = {
    Response.ok.entity(grokRepo.insertGrok(grok)).build()
  }

  @POST
  @Path("insert-grok-batch")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertGrok(groks: List[Grok]): Response = {
    Response.ok.entity(grokRepo.insertGrok(groks)).build()
  }

  @GET
  @Path("delete-grok-by-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteGrokById(@PathParam("id") id: Long): Response = {
    Response.ok.entity(grokRepo.deleteGrokById(id)).build()
  }

  @GET
  @Path("delete-grok-by-meta-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteGrokByMetaDataId(@PathParam("metaDataId") metaDataId: Long): Response = {
    Response.ok.entity(grokRepo.deleteGrokByMetaDataId(metaDataId)).build()
  }

  @GET
  @Path("find-all-grok-log-msgs")
  @Produces(Array(APPLICATION_JSON))
  def findAllGrokLogMsg(): Response = {
    Response.ok.entity(grokRepo.findAllGrokLogMsg()).build()
  }

  @GET
  @Path("find-grok--by-log-msg-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findGrokLogMsgById(@PathParam("id") id: Long): Response = {
    Response.ok.entity(grokRepo.findGrokLogMsgById(id)).build()
  }

  @POST
  @Path("insert-grok-log-msg")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertGrokLogMsg(logMessages: GrokLogMessages): Response = {
    Response.ok.entity(grokRepo.insertGrokLogMsg(logMessages)).build()
  }

  @POST
  @Path("insert-grok-log-msg-list")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertGrokLogMsg(logMessages: List[GrokLogMessages]): Response = {
    Response.ok.entity(grokRepo.insertGrokLogMsg(logMessages)).build()
  }

  @POST
  @Path("delete-grok-log-msg-by-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteGrokLogMsgById(@PathParam("id") id: Long): Response = {
    Response.ok.entity(grokRepo.deleteGrokLogMsgById(id)).build()
  }

  @POST
  @Path("delete-grok-log-msg-by-grok-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteGrokLogMsgByGrokId(@PathParam("id") grokId: Long): Response = {
    Response.ok.entity(grokRepo.deleteGrokLogMsgByGrokId(grokId)).build()
  }

}
