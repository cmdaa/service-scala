package io.cmdaa.service.resource

import java.util.Calendar

import com.google.gson.{Gson, GsonBuilder}
import com.typesafe.scalalogging.Logger
import io.cmdaa.service.auth.AppUser
import io.dropwizard.auth.Auth
import javax.annotation.security.RolesAllowed
import javax.ws.rs._
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response
import io.cmdaa.data.dao.{ServerRepo, UserProjectRepo}
import io.cmdaa.data.util.entity.ServerSchema.{Label, Server}
import io.cmdaa.data.util.OptionSerializer

import scala.collection.mutable.ListBuffer

@Path("/")
class ServerResource(serverRepo: ServerRepo, userProjectRepo: UserProjectRepo) extends AbstractResource(userProjectRepo: UserProjectRepo) {
  val gsonb = new GsonBuilder()
  gsonb.registerTypeAdapter(classOf[Option[Any]], new OptionSerializer)
  val gson: Gson = gsonb.create

  val logger: Logger = Logger[ServerResource]

  @RolesAllowed(Array("ADMIN"))
  @GET
  @Path("label")
  @Produces(Array(APPLICATION_JSON))
  def listLabels(@Auth principal: AppUser): Response = {
    Response.ok.entity(serverRepo.listLabels()).build()
  }

  @GET
  @Path("label/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findLabel(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    val label: Option[Label] = serverRepo.findLabel(id)
    if (label.isEmpty) {
      Response.status(Response.Status.NOT_FOUND).build()
    } else {
      Response.ok.entity(label).build()
    }
  }

  @POST
  @Path("label")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertLabel(@Auth principal: AppUser, in: Map[String, Any]): Response = {

    val label: Label = Label(
      None,
      in.getOrElse("name", "").toString,
      who(principal),
      Calendar.getInstance().getTime.getTime
    )
    Response.ok.entity(serverRepo.insertLabel(label)).build()
  }

  @PUT
  @Path("label")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def updateLabel(@Auth principal: AppUser, in: Map[String, Any]): Response = {

    val label: Label = Label(
      Some(in("id").asInstanceOf[Int].asInstanceOf[Long]),
      in.getOrElse("name", "").toString,
      who(principal),
      Calendar.getInstance().getTime.getTime
    )
    Response.ok.entity(serverRepo.updateLabel(label)).build()
  }

  @DELETE
  @Path("label/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteLabel(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(serverRepo.deleteLabel(id)).build()
  }

  @GET
  @Path("server")
  @Produces(Array(APPLICATION_JSON))
  def listServers(@Auth principal: AppUser): Response = {
    Response.ok.entity(serverRepo.listServers()).build()
  }

  @GET
  @Path("server/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findServer(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    val server: Option[Server] = serverRepo.findServer(id)
    if (server.isEmpty) {
      Response.status(Response.Status.NOT_FOUND).build()
    } else {
      Response.ok.entity(server).build()
    }
  }

  @POST
  @Path("server")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertServer(@Auth principal: AppUser, in: Map[String, Any]): Response = {

    val server: Server = Server(
      None,
      in.getOrElse("fqdn", "").toString,
      in.getOrElse("ipAddrV4", "").toString,
      in.getOrElse("ipAddrV6", "").toString,
      who(principal),
      Calendar.getInstance().getTime.getTime
    )
    val labels = new ListBuffer[Label]()
    val labelMaps = in.getOrElse("labels", List.empty[Map[String, Any]]).asInstanceOf[List[Map[String, Any]]]

    for (labelMap <- labelMaps) {
      labels += Label(
        Some(labelMap("id").asInstanceOf[Int].asInstanceOf[Long]),
        labelMap.getOrElse("name", "").toString,
        "modifier",
        Calendar.getInstance().getTime.getTime
      )
    }
    Response.ok.entity(serverRepo.insertServer(server, labels.toList)).build()
  }

  @PUT
  @Path("server")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def updateServer(@Auth principal: AppUser, in: Map[String, Any]): Response = {

    val server: Server = Server(
      Some(in("id").asInstanceOf[Int].asInstanceOf[Long]),
      in.getOrElse("fqdn", "").toString,
      in.getOrElse("ipAddrV4", "").toString,
      in.getOrElse("ipAddrV6", "").toString,
      who(principal),
      Calendar.getInstance().getTime.getTime
    )
    val labels = new ListBuffer[Label]()
    val labelMaps = in.getOrElse("labels", List.empty[Map[String, Any]]).asInstanceOf[List[Map[String, Any]]]

    for (labelMap <- labelMaps) {
      labels += Label(
        Some(labelMap("id").asInstanceOf[Int].asInstanceOf[Long]),
        labelMap.getOrElse("name", "").toString,
        "modifier",
        Calendar.getInstance().getTime.getTime
      )
    }

    Response.ok.entity(serverRepo.updateServer(server, labels.toList)).build()
  }

  @DELETE
  @Path("server/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteServer(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(serverRepo.deleteServer(id)).build()
  }

  @GET
  @Path("server/{id}/label")
  @Produces(Array(APPLICATION_JSON))
  def listLabelsForServer(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(serverRepo.findLabelsByServer(id)).build()
  }

  @GET
  @Path("server/find-all-servers")
  @Produces(Array(APPLICATION_JSON))
  def findAllServers(): Response = {
    Response.ok.entity(serverRepo.listServers()).build()
  }

  @GET
  @Path("server/find-server-by-id/{serverId}")
  @Produces(Array(APPLICATION_JSON))
  def findServerById(@PathParam("serverId") serverId: Long): Response = {
    Response.ok.entity(serverRepo.findServer(serverId)).build()
  }

  @GET
  @Path("server/delete-server-by-id/{serverId}")
  @Produces(Array(APPLICATION_JSON))
  def deleteServerById(@PathParam("serverId") serverid: Long): Response = {
    Response.ok.entity(serverRepo.deleteServer(serverid)).build()
  }

  @GET
  @Path("server/find-all-server-labels")
  @Produces(Array(APPLICATION_JSON))
  def findAllServersLabels(): Response = {
    Response.ok.entity(serverRepo.listLabels()).build()
  }

  @GET
  @Path("server/find-server-by-label/{labelId}")
  @Produces(Array(APPLICATION_JSON))
  def findServerLabelById(@PathParam("labelId") labelId: Long): Response = {
    Response.ok.entity(serverRepo.findLabel(labelId)).build()
  }

  @GET
  @Path("server/delete-by-server-label/{labelId}")
  @Produces(Array(APPLICATION_JSON))
  def deleteByServerLabelId(@PathParam("labelId") labelId: Long): Response = {
    Response.ok.entity(serverRepo.deleteLabel(labelId)).build()
  }

  @GET
  @Path("server/server-by-label/{labelId}")
  @Produces(Array(APPLICATION_JSON))
  def getServersByLabel(@PathParam("labelId") labelId: Long): Response = {
    Response.ok.entity(serverRepo.findServersByLabel(labelId)).build()
  }

  @GET
  @Path("server/get-labels-for-server/{serverId}")
  @Produces(Array(APPLICATION_JSON))
  def getLabelsForServer(@PathParam("serverId") serverId: Long): Response = {
    Response.ok.entity(serverRepo.findLabelsByServer(serverId)).build()
  }

  @GET
  @Path("server/insert-server/{server}")
  @Produces(Array(APPLICATION_JSON))
  def insertServer(@PathParam("server") server: String): Response = {
    Response.ok.entity(serverRepo.insertServer(gson.fromJson(server, classOf[Server]))).build()
  }

  @GET
  @Path("server/insert-server-label/{label}")
  @Produces(Array(APPLICATION_JSON))
  def insertLabel(@PathParam("label") label: String): Response = {
    Response.ok.entity(serverRepo.insertLabel(gson.fromJson(label, classOf[Label]))).build()
  }

}
