package io.cmdaa.service.resource

import io.cmdaa.service.auth.AppUser
import io.cmdaa.data.dao.UserProjectRepo

abstract class AbstractResource(userProjectRepo: UserProjectRepo) {

  protected def who(principal: AppUser): String = {
      val user = userProjectRepo.findUser(principal.id)
      var modifier: String = "Unknown"
      if (user.nonEmpty) {
        modifier = user.get.username
      }
      modifier
  }

}
