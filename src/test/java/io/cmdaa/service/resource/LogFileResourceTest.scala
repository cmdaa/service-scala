package io.cmdaa.service.resource

import com.google.gson.Gson
import org.junit.runner.RunWith
import io.cmdaa.data.util.entity.LogFileSchema.LogFile
import io.cmdaa.data.util.entity.LogFileSchema.LogSource
import org.scalamock.scalatest.MockFactory
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}
import io.cmdaa.data.dao.{GrokRepo, LogFileRepo, UserProjectRepo}

@RunWith(classOf[JUnitRunner])
class LogFileResourceTest extends FlatSpec with MockFactory with BeforeAndAfter {
//  val db = mock[Database]
  val logFile = LogFile(1L,"messages","/var/log/messages",1L)
  val logSource = LogSource(Some(1L),"messages",1L,Some(200L), "/var/log/messages", 1L,"me",1L)
  val listLogFile = scala.List(logFile, logFile)
  val userProjectRepo: UserProjectRepo = stub[UserProjectRepo]
  val userProjectResource = new UserProjectResource(userProjectRepo)
  val logFileRepo: LogFileRepo = stub[LogFileRepo]
  val grokRepo: GrokRepo = stub[GrokRepo]
  val logFileResource = new LogFileResource(logFileRepo, grokRepo, userProjectRepo)


  "findById" should "return a single element" in {
    (logFileRepo.findById _).when(1).returns(Some(logSource))
    assert(logFileResource.findById(1).getEntity === logSource)
  }

  "findByFileName" should "return a single element" in {
    (logFileRepo.findByFileName _).when("/var/log/messages").returns(Some(logSource))
   // assert(logFileResource.findByFileName( logFile = "{\"fileName\":\"/var/log/messages\"}").getEntity == logFile)
    assert(logFileResource.findByFileName( "{\"fileName\":\"/var/log/messages\"}").getEntity == logSource)
  }

  "deleteById" should "return number deleted" in {
    (logFileRepo.deleteById _).when(1).returns(1)
    assert(logFileResource.deleteById(1).getEntity === 1)
  }

  "findAll" should "return a list" in {
    (logFileRepo.findAll _).when().returning(listLogFile)
    assert(logFileResource.findAll.getEntity == listLogFile)
  }

  "findFilesOwnedByGroup" should "return a list" in {
    (logFileRepo.findFilesOwnedByProject _).when(1L).returning(listLogFile)
    assert(logFileResource.findFilesOwnedByProject(1).getEntity === listLogFile)
  }

  "jsontest" should "test json" in {
    val gson = new Gson()
    val str = gson.toJson(logFile)
    println(str)
    val v = gson.fromJson(str,classOf[LogFile])
    println(v)
  }

}
