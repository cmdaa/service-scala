package io.cmdaa.service

import com.fasterxml.jackson.databind.{ObjectMapper, SerializationFeature}
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.typesafe.scalalogging.Logger
import io.cmdaa.data.Version
import io.cmdaa.data.dao._
import io.cmdaa.service.auth.{AppAuthenticator, AppAuthorizer, AppUser}
import io.cmdaa.service.resource._
import io.dropwizard.Application
import io.dropwizard.auth.oauth.OAuthCredentialAuthFilter
import io.dropwizard.auth.{AuthDynamicFeature, AuthValueFactoryProvider}
import io.dropwizard.setup.Environment
import javax.servlet.DispatcherType
import org.atmosphere.cpr.{ApplicationConfig, AtmosphereServlet}
import org.eclipse.jetty.servlets.CrossOriginFilter
import org.eclipse.jetty.servlets.CrossOriginFilter.{ALLOWED_HEADERS_PARAM, ALLOWED_METHODS_PARAM, ALLOWED_ORIGINS_PARAM}
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.PostgresProfile

class CmdaaApp() extends Application[CmdaaAppConfig] {

  val logger: Logger = Logger[CmdaaApp]

  override def getName: String = "cmda-services"

  override def run(t: CmdaaAppConfig, env: Environment): Unit = {
    val db = Database.forConfig("postgresql")

    val userProjectRepo = new UserProjectRepo(PostgresProfile, db)
    val logFileRepo: LogFileRepo = new LogFileRepo(PostgresProfile, db)
    val grokRepo: GrokRepo = new GrokRepo(PostgresProfile, db)

    logger.info(s"Token Expire Time in ms = ${t.accessTokenExpireTimeMillis}")
    logger.info(s"Data Layer version is ${Version.major}.${Version.minor}")


    env.jersey().register(new RootResource)
    env.jersey().register(new LogFileResource(logFileRepo, grokRepo, userProjectRepo))
    env.jersey().register(new LogLikelihoodResource(new LogLikelihoodAnalyticRepo(PostgresProfile, db)))
    env.jersey().register(new LogLineResource(new LogLineRepo(PostgresProfile, db)))
    env.jersey().register(new LogLineCountResource(new LogLineCountRepo(PostgresProfile, db)))
    env.jersey().register(new ServerResource(new ServerRepo(PostgresProfile, db), userProjectRepo))
    env.jersey().register(new UserProjectResource(userProjectRepo))
    env.jersey().register(new GrokResource(grokRepo, userProjectRepo))
    env.jersey().register(new AuthResource(userProjectRepo, t.allowedGrantTypes))
    env.jersey.register(jacksonJaxbJsonProvider)

    env.jersey().register(new CreateDeleteDbResource(PostgresProfile, db))

    env.jersey().register(new AuthDynamicFeature(
      new OAuthCredentialAuthFilter.Builder()
        .setAuthenticator(new AppAuthenticator(userProjectRepo, t.accessTokenExpireTimeMillis))
        .setAuthorizer(new AppAuthorizer())
        .setPrefix("Bearer")
        .buildAuthFilter()))
    env.jersey.register(classOf[RolesAllowedDynamicFeature])
    env.jersey.register(new AuthValueFactoryProvider.Binder(classOf[AppUser]))

    // Enable CORS headers
    val cors = env.servlets.addFilter("CORS", classOf[CrossOriginFilter])

    // Configure CORS parameters
    cors.setInitParameter(ALLOWED_ORIGINS_PARAM, "*")
    cors.setInitParameter(ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,Authorization")
    cors.setInitParameter(ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD")

    // Add URL mapping
    cors.addMappingForUrlPatterns(java.util.EnumSet.allOf(classOf[DispatcherType]), true, "/*")

    val servlet = new AtmosphereServlet
    servlet.framework.addInitParameter("com.sun.jersey.config.property.packages", "io.cmdaa.service.resource.websocket")
    servlet.framework.addInitParameter(ApplicationConfig.WEBSOCKET_CONTENT_TYPE, "application/json")
    servlet.framework.addInitParameter(ApplicationConfig.WEBSOCKET_SUPPORT, "true")
    val servletHolder = env.servlets.addServlet("AtmosphereService", servlet)
    servletHolder.addMapping("/ws/*")

  }

  private def jacksonJaxbJsonProvider: JacksonJaxbJsonProvider = {
    val provider = new JacksonJaxbJsonProvider()
    val objectMapper = new ObjectMapper()
    objectMapper.registerModule(DefaultScalaModule)
    objectMapper.registerModule(new JodaModule)
    objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false)
    objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
    objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
    provider.setMapper(objectMapper)
    provider
  }
}

object CmdaaApp {
  def main(args: Array[String]): Unit = new CmdaaApp().run(args: _*)
}
