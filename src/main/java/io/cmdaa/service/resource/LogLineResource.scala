package io.cmdaa.service.resource

import com.google.gson.GsonBuilder
import io.cmdaa.data.dao.LogLineRepo
import io.cmdaa.data.util.entity.LogFileSchema.LogLine
import io.cmdaa.data.util.OptionSerializer
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response
import javax.ws.rs.{GET, Path, PathParam, Produces}

@Path("/log-line")
class LogLineResource(logLineRepo: LogLineRepo) {
  val gsonb = new GsonBuilder()
  gsonb.registerTypeAdapter(classOf[Option[Any]], new OptionSerializer)
  val gson = gsonb.create

  @GET
  @Path("find-all")
  @Produces(Array(APPLICATION_JSON))
  def findAll(): Response = {
      Response.ok.entity(logLineRepo.findAll().toList).build()
  }

  @GET
  @Path("find-by-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findById(@PathParam("id") id: Long): Response = {
       Response.ok.entity(logLineRepo.findById(id)).build()
  }

  @GET
  @Path("find-all-lines-by-log-file/{logFileId}")
  @Produces(Array(APPLICATION_JSON))
  def findAllLinesByLogFile(@PathParam("logFileId")logFileId: Long): Response = {
      Response.ok.entity(logLineRepo.findAllLinesByLogFile(logFileId).toList).build()
  }

  @GET
  @Path("delete-by-log-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteByLogId(@PathParam("id") id: Long): Response = {
      Response.ok.entity(logLineRepo.deleteByLogId(id)).build()
  }

  @GET
  @Path("insert/{list}/{logLines}")
  @Produces(Array(APPLICATION_JSON))
  def insert(@PathParam("list") list: Boolean, @PathParam("logLines") logLines:  String): Response = {
      if (list) {
        Response.ok.entity(logLineRepo.insert(gson.fromJson(logLines, classOf[Stream[LogLine]]))).build()
      } else {

        println(logLines)
        val v = gson.fromJson(logLines, classOf[LogLine])
        println(v)
        Response.ok.entity(logLineRepo.insert(gson.fromJson(logLines, classOf[LogLine]))).build()
      }
  }

  @GET
  @Path("mark-record-as-retain/{id}")
  @Produces(Array(APPLICATION_JSON))
  def markRecordAsRetain(@PathParam("id") id: Long): Response = {
      Response.ok.entity(logLineRepo.markRecordAsRetain(id)).build()
  }
}