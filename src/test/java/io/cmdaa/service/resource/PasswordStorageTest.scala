package io.cmdaa.service.resource

import io.cmdaa.service.util.PasswordStorage
import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfter, FlatSpec}
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PasswordStorageTest extends FlatSpec with BeforeAndAfter{

  "hashPassword" should "hash a password and correctly verify" in {
    val password: String = "adfaedfadsf45%6w!"
    val passwordHash: String = PasswordStorage.createHash(password)
    assert(PasswordStorage.verifyPassword(password, passwordHash))

    val badPassword: String = "badpassword"
    assert(!PasswordStorage.verifyPassword(badPassword, passwordHash))
  }

}
