#!/usr/bin/env bash
mvn clean install
mvn dockerfile:build
kubectl delete -f ../kubernetes-configs/developer-env/cmdaa/cmdaa.yml
kubectl create -f ../kubernetes-configs/developer-env/cmdaa/cmdaa.yml
