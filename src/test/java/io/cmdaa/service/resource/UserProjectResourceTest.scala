package io.cmdaa.service.resource

import com.google.gson.{Gson, GsonBuilder}
import io.cmdaa.service.auth.AppUser
import javax.ws.rs.core.Response.Status
import org.junit.runner.RunWith
import io.cmdaa.data.dao.UserProjectRepo
import io.cmdaa.data.util.entity.UserSchema.{Project, User}
import io.cmdaa.data.util.OptionSerializer
import org.scalamock.scalatest.MockFactory
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}

@RunWith(classOf[JUnitRunner])
class UserProjectResourceTest extends FlatSpec with MockFactory with BeforeAndAfter {

  val user = User(Some(1L), "jdoe@test.com", "password", "John", "Doe", "jdoe@test.com", active = true, approved = false, "modifier", 10000)
  val project = Project(Some(1L), "cmdaa", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 20000)
  val principal: AppUser = new AppUser(1L, "principal", Set("ADMIN"))
  val userProjectRepo: UserProjectRepo = stub[UserProjectRepo]
  val userProjectResource = new UserProjectResource(userProjectRepo)
  val gsonb = new GsonBuilder()
  gsonb.registerTypeAdapter(classOf[Option[Any]], new OptionSerializer)
  val gson: Gson = gsonb.create

  "inserting a user" should "succeed" in {
    val u: User = User(None, "jdoe", "thepassword", "John", "Doe", "jdoe@foo.bar", active = true, approved = true, "modifier", 10000)
    val m = Map(("id", None), ("username", "jdoe"), ("password", "thepassword"), ("firstName", "John"), ("lastName", "Doe"))
    (userProjectRepo.insertUser(_: User)).when(*).returns(u)
    (userProjectRepo.findUser _).when(1).returns(Some(u))
    val resp = userProjectResource.insertUser(principal, m)
    assert(Status.OK.getStatusCode === resp.getStatus)
    assert(resp.getEntity === u)
  }

  "findUser" should "" in {
    (userProjectRepo.findUser _).when(1).returns(Some(user))
    assert(userProjectResource.findUser(principal, 1).getEntity === Some(user))
  }

  "listUsers" should "" in {
    (userProjectRepo.listUsers _).when().returns(List(user))
    assert(userProjectResource.listUsers(principal).getEntity === List(user))
  }

}
