package io.cmdaa.service.resource

import java.util.Calendar

import com.google.gson.{Gson, GsonBuilder}
import io.cmdaa.service.auth.AppUser
import io.dropwizard.auth.Auth
import javax.annotation.security.RolesAllowed
import javax.ws.rs._
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response
import io.cmdaa.data.dao.{GrokRepo, LogFileRepo, UserProjectRepo}
import io.cmdaa.data.util.entity.GrokSchema.{Grok, GrokLogMessages}
import io.cmdaa.data.util.entity.LogFileSchema.{LogFileName, LogSource, LogSourceType}
import io.cmdaa.data.util.OptionSerializer

//import dropwizard.scala.example.api.{User, UserPojo, UserPojoWithJaxbAnnotation, UserWithJaxbAnnotation}
//import io.cmdaa.service.UserWithJaxbAnnotation

@Path("/")
class LogFileResource(logFileRepo: LogFileRepo, grokRepo: GrokRepo, userProjectRepo: UserProjectRepo) extends AbstractResource(userProjectRepo: UserProjectRepo) {

  val gsonb = new GsonBuilder()
  gsonb.registerTypeAdapter(classOf[Option[Any]], new OptionSerializer)
  val gson: Gson = gsonb.create
  //  val db = Database.forConfig("h2" )
  //  val logFileRepo = new LogFileRepo(H2Profile, db)

  //  @GET
  //  @Produces(Array(APPLICATION_JSON))
  //  def default: Response = {
  //    Response.ok.entity(Map("message" -> "Scala rocks!", "today" -> DateTime.now)).build()
  //  }
  //
  //  @GET
  //  @Path("scala/json")
  //  @Produces(Array(APPLICATION_JSON))
  //  def jsonAndScalaEntity: Response = {
  //    Response.ok.entity(new User("me", "me@example.com")).build()
  //  }

  //  @GET
  //  @Path("scala/xml")
  //  @Produces(Array(APPLICATION_XML))
  //  def xmlAndScalaEntity: Response = {
  //    Response.ok.entity(new UserWithJaxbAnnotation("me", "me@example.com")).build()
  //  }
  //
  //  @GET
  //  @Path("scala")
  //  @Produces(Array(APPLICATION_JSON, APPLICATION_XML))
  //  def scalaEntity: Response = {
  //    Response.ok.entity(new UserWithJaxbAnnotation("me", "me@example.com")).build()
  //  }

  @GET
  @Path("log-file/find-by-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findById(@PathParam("id") id: Long): Response = {
    Response.ok.entity(logFileRepo.findById(id).getOrElse("")).build()
  }

  @GET
  //The :.* after the logFile allows us to include the / in the url.
  //to be safe with the use of this is is included in a json.  If the json doesn't match everything should fail
  @Path("log-file/find-by-filename/{logFile:.*}")
  @Produces(Array(APPLICATION_JSON))
  def findByFileName(@PathParam("logFile") logFile: String): Response = {
    val logFileName = gson.fromJson(logFile, classOf[LogFileName])
    Response.ok.entity(logFileRepo.findByFileName(logFileName.fileName).getOrElse("")).build()
  }

  @GET
  //The :.* after the logFile allows us to include the / in the url.
  //to be safe with the use of this is is included in a json.  If the json doesn't match everything should fail
  @Path("log-file/find-by-filename-like/{logFile:.*}")
  @Produces(Array(APPLICATION_JSON))
  def findByFileNameLike(@PathParam("logFile") logFile: String): Response = {
    val logFileName = gson.fromJson(logFile, classOf[LogFileName])
    Response.ok.entity(logFileRepo.findByFileNameLike(logFileName.fileName).getOrElse("")).build()
  }

  @GET
  @Path("log-file/find-all")
  @Produces(Array(APPLICATION_JSON))
  def findAll: Response = {
    Response.ok.entity(logFileRepo.findAll()).build()
  }

  @GET
  @Path("log-file/delete-by-id/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteById(@PathParam("id") id: Long): Response = {
    Response.ok.entity(logFileRepo.deleteById(id)).build()
  }

  @GET
  @Path("log-file/find-files-owned-by-project/{projectId}")
  @Produces(Array(APPLICATION_JSON))
  def findFilesOwnedByProject(@PathParam("projectId") projectId: Long): Response = {
    Response.ok.entity(logFileRepo.findFilesOwnedByProject(projectId)).build()
  }

  @GET
  @Path("logSource/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findLogSource(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    val logSource: Option[LogSource] = logFileRepo.findLogSource(id)
    if (logSource.isEmpty) {
      Response.status(Response.Status.NOT_FOUND).build()
    } else {
      Response.ok.entity(logSource).build()
    }
  }

  @GET
  @Path("logSourceType")
  @Produces(Array(APPLICATION_JSON))
  def listLogSourceTypes(@Auth principal: AppUser): Response = {
    Response.ok.entity(logFileRepo.listLogSourceTypes()).build()
  }

  @GET
  @Path("logSourceType/{id}")
  @Produces(Array(APPLICATION_JSON))
  def findLogSourceType(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    val logSourceType: Option[LogSourceType] = logFileRepo.findLogSourceType(id)
    if (logSourceType.isEmpty) {
      Response.status(Response.Status.NOT_FOUND).build()
    } else {
      Response.ok.entity(logSourceType).build()
    }
  }

  @RolesAllowed(Array("ADMIN"))
  @POST
  @Path("logSourceType")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def insertLogSourceType(@Auth principal: AppUser, in: Map[String, Any]): Response = {
    val logSourceType: LogSourceType = LogSourceType(
      None,
      in.getOrElse("name", "").toString,
      in.getOrElse("metaDef", "").toString,
      who(principal),
      Calendar.getInstance().getTime.getTime
    )
    Response.ok.entity(logFileRepo.insertLogSourceType(logSourceType)).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @PUT
  @Path("logSourceType")
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  def updateLogSourceType(@Auth principal: AppUser, in: Map[String, Any]): Response = {
    val logSourceType: LogSourceType = LogSourceType(
      Some(in("id").asInstanceOf[Int].asInstanceOf[Long]),
      in.getOrElse("name", "").toString,
      in.getOrElse("metaDef", "").toString,
      who(principal),
      Calendar.getInstance().getTime.getTime
    )
    Response.ok.entity(logFileRepo.updateLogSourceType(logSourceType)).build()
  }

  @RolesAllowed(Array("ADMIN"))
  @DELETE
  @Path("logSourceType/{id}")
  @Produces(Array(APPLICATION_JSON))
  def deleteLogSourceType(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    Response.ok.entity(logFileRepo.deleteLogSourceType(id)).build()
  }

  @GET
  @Path("logSource/{id}/type")
  @Produces(Array(APPLICATION_JSON))
  def findLogSourceTypeForLogSource(@Auth principal: AppUser, @PathParam("id") id: Long): Response = {
    val logSource: Option[LogSource] = logFileRepo.findLogSource(id)
    if (logSource.isEmpty) {
      Response.status(Response.Status.NOT_FOUND).build()
    } else {
      val logSourceType: Option[LogSourceType] = logFileRepo.findLogSourceType(logSource.get.typeId)
      if (logSourceType.isEmpty) {
        Response.status(Response.Status.NOT_FOUND).build()
      } else {
        Response.ok.entity(logSourceType).build()
      }
    }
  }

  @GET
  @Path("logSource/{id}/groks")
  @Produces(Array(APPLICATION_JSON))
  def listLogSourceGroks(@Auth principal: AppUser, @PathParam("id") id: Long, @QueryParam("offset") offset: Long, @QueryParam("limit") limit: Long): Response = {
    case class Resp(count: Long, groks: List[Grok])

    var pagination: Option[(Long, Long)] = None
    if (limit > 0) {
      pagination = Some(offset, limit)
    }
    val res: (Int, List[Grok]) = grokRepo.listLogSourceGroks(id, pagination)
    Response.ok.entity(Resp(res._1, res._2)).build
  }

  @GET
  @Path("logSource/{sourceId}/grok/{grokId}/messages")
  @Produces(Array(APPLICATION_JSON))
  def listLogMessagesForGrok(@Auth principal: AppUser, @PathParam("sourceId") sourceId: Long, @PathParam("grokId") grokId: Long, @QueryParam("offset") offset: Long, @QueryParam("limit") limit: Long): Response = {
    case class Resp(count: Long, messages: List[GrokLogMessages])

    var pagination: Option[(Long, Long)] = None
    if (limit > 0) {
      pagination = Some(offset, limit)
    }
    val res: (Int, List[GrokLogMessages]) = grokRepo.listLogSourceGrokLogsByGrokId(sourceId, grokId, pagination)
    Response.ok.entity(Resp(res._1, res._2)).build
  }

}