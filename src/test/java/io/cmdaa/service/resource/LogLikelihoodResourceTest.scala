package io.cmdaa.service.resource

import com.google.gson.Gson
import org.junit.runner.RunWith
import io.cmdaa.data.dao.LogLikelihoodAnalyticRepo
import io.cmdaa.data.util.entity.LogFileSchema.LogFile
import io.cmdaa.data.util.entity.LogLikelihoodSchema.LogLikeLihoodRun
import org.scalamock.scalatest.MockFactory
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}

@RunWith(classOf[JUnitRunner])
class LogLikelihoodResourceTest extends FlatSpec with MockFactory with BeforeAndAfter {

  val logFile = LogFile(1L, "messages", "/var/log/messages", 1)
  val logLikelihoodRun = LogLikeLihoodRun(1L, 0.90D, 0.05D, 1524244391L, 1L)
  val listLLRuns = List(LogLikeLihoodRun(1L, 0.90D, 0.05D, 1524244391L, 1L), LogLikeLihoodRun(2L, 0.90D, 0.05D, 1524244391L, 1L))
  val llRunsWithLogFile = List((LogLikeLihoodRun(1L, 0.90D, 0.05D, 1524244391L, 1L), logFile), (LogLikeLihoodRun(2L, 0.90D, 0.05D, 1524244391L, 1L), logFile))
  
  val llRunRepo: LogLikelihoodAnalyticRepo = stub[LogLikelihoodAnalyticRepo]
  val llResource = new LogLikelihoodResource(llRunRepo)

  val gson = new Gson()

  "findAllRunsByLogFile" should "return a list" in {
    (llRunRepo.findAllRunsByLogFile _).when(1).returning(listLLRuns)
    assert(llResource.findAllRunsByLogFile(1).getEntity == listLLRuns)
  }

  "deleteById" should "return number deleted" in {
    (llRunRepo.deleteById _).when(1).returns(1)
    assert(llResource.deleteById(1).getEntity === 1)
  }

  "deleteByLogId" should "delete multiple records" in {
    (llRunRepo.deleteByLogId _).when(1).returns(5)
    assert(llResource.deleteByLogId(1).getEntity === 5)
  }

  "findAll" should "find all runs" in {
    (llRunRepo.findAll _).when().returning(listLLRuns)
    assert(llResource.findAll().getEntity === listLLRuns)
  }

  "findAllWithLogFile" should "return a list" in {
    (llRunRepo.findAllWithLogFile _).when().returning(llRunsWithLogFile)
    assert(llResource.findAllWithLogFile().getEntity === llRunsWithLogFile)
  }

  "insert object" should "insert a single object" in {
    (llRunRepo.insert(_: LogLikeLihoodRun)).when(logLikelihoodRun).returning()
    assert((llResource insert(false, logLikeLihoodRun = gson.toJson(logLikelihoodRun))).getEntity === ())
  }

  "insert list" should "insert a list of objects" in {
    (llRunRepo.insert(_: List[LogLikeLihoodRun])).when(listLLRuns).returning()
    assert((llResource insert(false, gson.toJson(listLLRuns))).getEntity === ())
  }
}
