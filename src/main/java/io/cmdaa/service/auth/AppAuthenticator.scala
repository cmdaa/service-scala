package io.cmdaa.service.auth

import java.util.{Calendar, Optional, UUID}

import io.dropwizard.auth.Authenticator
import io.cmdaa.data.dao.UserProjectRepo

class AppAuthenticator(val repo: UserProjectRepo, accessTokenExpireTimeMillis: Long) extends Authenticator[String, AppUser] {

  override def authenticate(accessTokenId: String): Optional[AppUser] = {

    var ret: Optional[AppUser] = Optional.empty[AppUser]

    try {
      // Check input, must be a valid UUID
      UUID.fromString(accessTokenId)

      // Get the access token from the database
      val tokenOption = this.repo.findAccessToken(accessTokenId)
      if (tokenOption.nonEmpty) {
        val token = tokenOption.get
        val nowMillis = Calendar.getInstance.getTime.getTime

        // Check if the last access time is not too far in the past (the access token is expired)
        if ((nowMillis - token.lastAccessDateMillis) <= accessTokenExpireTimeMillis) {

          // Update the access time for the token
          this.repo.touchAccessToken(accessTokenId)

          // Return the user principal for processing
          val userOption = this.repo.findUser(token.userId)
          if (userOption.nonEmpty) return Optional.of(new AppUser(userOption.get.id.get, userOption.get.username, Set("ADMIN")))

        } else {
          this.repo.deleteAccessToken(accessTokenId)
        }
      }
    } catch {
      case _: IllegalArgumentException =>
        ret = Optional.empty[AppUser]
    }

    ret

  }
}
