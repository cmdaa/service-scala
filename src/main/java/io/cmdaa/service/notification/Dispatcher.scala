package io.cmdaa.service.notification

trait Dispatcher[A] {
  def dispatch(payload: A): Unit
}
